from django import forms
from lab_2.models import Note


class NoteForm(forms.ModelForm):
    class Meta:
        # form from Note model
        model = Note
        # all fields in model should be used
        fields = "__all__"