from django.test import TestCase
from django.test import Client

# Create tests
class Lab4UnitTest(TestCase):
    def test_lab4_is_exist(self):
        response = Client().get("/lab-4/")
        self.assertEqual(response.status_code, 200)

    def test_add_is_exist(self):
        response = Client().get("/lab-4/add-note")
        self.assertEqual(response.status_code, 200)

    def test_card_is_exist(self):
        response = Client().get("/lab-4/note-list")
        self.assertEqual(response.status_code, 200)