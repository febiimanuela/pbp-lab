import 'package:flutter/material.dart';
import 'washyourlyrics_screen.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Image.asset('images/project-header.png'),
            backgroundColor: Colors.green[300]),
        body: Container(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <
              Widget>[
            Image.asset('images/project-title.png'),
            const Text(
              "Teman untuk kalian di tengah kondisi pandemi :D",
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 18.0,
              ),
              textAlign: TextAlign.center,
            ),
            Container(
              margin: const EdgeInsets.all(20.0),
              child: ElevatedButton(
                child: const Text("Wash Your Lyrics"),
                style: TextButton.styleFrom(backgroundColor: Colors.green[300]),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return WashYourLyrics();
                  }));
                },
              ),
            ),
          ]),
          margin: const EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 0.0),
        ));
  }
}
