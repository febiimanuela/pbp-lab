import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class WashYourLyrics extends StatefulWidget {
  @override
  _WashYourLyricsState createState() => _WashYourLyricsState();
}

class _WashYourLyricsState extends State<WashYourLyrics> {
  TextEditingController controller1 = TextEditingController();
  TextEditingController controller2 = TextEditingController();
  TextEditingController controller3 = TextEditingController();
  TextEditingController controller4 = TextEditingController();
  TextEditingController controller5 = TextEditingController();
  TextEditingController controller6 = TextEditingController();
  TextEditingController controller7 = TextEditingController();
  TextEditingController controller8 = TextEditingController();
  TextEditingController controller9 = TextEditingController();
  String text1 = "";
  String text2 = "";
  String text3 = "";
  String text4 = "";
  String text5 = "";
  String text6 = "";
  String text7 = "";
  String text8 = "";
  String text9 = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Image.asset('images/project-header.png'),
          backgroundColor: Colors.green[300]),
      body: SingleChildScrollView(
          child: Container(
        margin: const EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            const Text(
              "Wash Your Lyrics",
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 48.0,
              ),
              textAlign: TextAlign.center,
            ),
            Container(
                margin: const EdgeInsets.all(20.0),
                child: Column(children: <Widget>[
                  const Text(
                    "Tahukah kamu bahwa diperlukan waktu selama minimal 20 detik untuk mencuci tangan hingga bersih? Kalau begitu, mari mencuci tangan sambil menyanyikan lagu kesukaanmu!",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16.0,
                    ),
                  ),
                  const Text(
                    "(versi orisinal: washyourlyrics.com)",
                  ),
                ])),
            Container(
              margin: const EdgeInsets.all(20.0),
              child: Image.asset('images/illustration.png'),
            ),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 1..."),
                controller: controller1),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 2..."),
                controller: controller2),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 3..."),
                controller: controller3),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 4..."),
                controller: controller4),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 5..."),
                controller: controller5),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 6..."),
                controller: controller6),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 7..."),
                controller: controller7),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 8..."),
                controller: controller8),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 9..."),
                controller: controller9),
            Container(
                margin: const EdgeInsets.all(20.0),
                child: Ink(
                    decoration: const ShapeDecoration(
                        shape: CircleBorder(), color: Colors.green),
                    child: IconButton(
                        icon: const Icon(Icons.send),
                        tooltip: "Submit",
                        onPressed: () {
                          setState(() {
                            text1 = controller1.text;
                            text2 = controller2.text;
                            text3 = controller3.text;
                            text4 = controller4.text;
                            text5 = controller5.text;
                            text6 = controller6.text;
                            text7 = controller7.text;
                            text8 = controller8.text;
                            text9 = controller9.text;
                          });
                        }))),
            Container(
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Column(children: <Widget>[
                  Image.asset('images/01.jpg'),
                  Text(text1),
                  Image.asset('images/02.jpg'),
                  Text(text2),
                  Image.asset('images/03.jpg'),
                  Text(text3),
                  Image.asset('images/04.jpg'),
                  Text(text4),
                  Image.asset('images/05.jpg'),
                  Text(text5),
                  Image.asset('images/06.jpg'),
                  Text(text6),
                  Image.asset('images/07.jpg'),
                  Text(text7),
                  Image.asset('images/08.jpg'),
                  Text(text8),
                  Image.asset('images/09.jpg'),
                  Text(text9),
                ])),
            Container(
              margin: const EdgeInsets.all(20.0),
              child: ElevatedButton(
                child: const Text("Done!"),
                style: TextButton.styleFrom(backgroundColor: Colors.green[300]),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            )
          ],
        ),
      )),
    );
  }
}
