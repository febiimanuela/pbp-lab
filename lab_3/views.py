from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create views
@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all().values()
    response = {"friends": friends}
    return render(request, "lab3_index.html", response)


@login_required(login_url="/admin/login/")
def add_friend(request):
    # request by POST
    form = FriendForm(request.POST)
    # save form data if valid
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/lab-3")

    response = {"form": form}

    return render(request, "lab3_form.html", response)