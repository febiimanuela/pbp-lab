from django import forms
from lab_1.models import Friend


class DateInput(forms.DateInput):
    input_type = "date"


class FriendForm(forms.ModelForm):
    class Meta:
        # form from Friend model
        model = Friend
        # all fields in model should be used
        fields = "__all__"
        widgets = {"DOB": DateInput()}