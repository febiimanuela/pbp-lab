import 'package:flutter/material.dart';
import 'package:lab_6/screens/homepage_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TemanCovid',
      theme: ThemeData(
        fontFamily: 'Inter',
      ),
      home: HomePage(),
    );
  }
}
