import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class WashYourLyrics extends StatefulWidget {
  @override
  _WashYourLyricsState createState() => _WashYourLyricsState();
}

class _WashYourLyricsState extends State<WashYourLyrics> {
  TextEditingController controller1 = TextEditingController();
  TextEditingController controller2 = TextEditingController();
  TextEditingController controller3 = TextEditingController();
  TextEditingController controller4 = TextEditingController();
  TextEditingController controller5 = TextEditingController();
  TextEditingController controller6 = TextEditingController();
  TextEditingController controller7 = TextEditingController();
  TextEditingController controller8 = TextEditingController();
  TextEditingController controller9 = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Image.asset('images/project-header.png'),
          backgroundColor: Colors.green[300]),
      body: SingleChildScrollView(
          child: Container(
        margin: const EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            const Text(
              "Wash Your Lyrics",
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 48.0,
              ),
              textAlign: TextAlign.center,
            ),
            const Text(
              "Tahukah kamu bahwa diperlukan waktu selama minimal 20 detik untuk mencuci tangan hingga bersih? Kalau begitu, mari mencuci tangan sambil menyanyikan lagu kesukaanmu!",
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 16.0,
              ),
            ),
            const Text(
              "(versi orisinal: washyourlyrics.com)",
            ),
            Image.asset('images/illustration.png'),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 1..."),
                onChanged: (value) {
                  setState(() {});
                },
                controller: controller1),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 2..."),
                onChanged: (value) {
                  setState(() {});
                },
                controller: controller2),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 3..."),
                onChanged: (value) {
                  setState(() {});
                },
                controller: controller3),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 4..."),
                onChanged: (value) {
                  setState(() {});
                },
                controller: controller4),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 5..."),
                onChanged: (value) {
                  setState(() {});
                },
                controller: controller5),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 6..."),
                onChanged: (value) {
                  setState(() {});
                },
                controller: controller6),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 7..."),
                onChanged: (value) {
                  setState(() {});
                },
                controller: controller7),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 8..."),
                onChanged: (value) {
                  setState(() {});
                },
                controller: controller8),
            TextField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Baris 9..."),
                onChanged: (value) {
                  setState(() {});
                },
                controller: controller9),
            Image.asset('images/01.jpg'),
            Text(controller1.text),
            Image.asset('images/02.jpg'),
            Text(controller2.text),
            Image.asset('images/03.jpg'),
            Text(controller3.text),
            Image.asset('images/04.jpg'),
            Text(controller4.text),
            Image.asset('images/05.jpg'),
            Text(controller5.text),
            Image.asset('images/06.jpg'),
            Text(controller6.text),
            Image.asset('images/07.jpg'),
            Text(controller7.text),
            Image.asset('images/08.jpg'),
            Text(controller8.text),
            Image.asset('images/09.jpg'),
            Text(controller9.text),
            ElevatedButton(
              child: const Text("Done!"),
              style: TextButton.styleFrom(backgroundColor: Colors.green[300]),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        ),
      )),
    );
  }
}
