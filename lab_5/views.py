from django.shortcuts import render
from lab_2.models import Note


def index(request):
    notes = Note.objects.all().values()
    response = {"notes": notes}
    return render(request, "lab5_index.html", response)

def get_note(request):
    notes = Note.objects.all().values()
    response = {"notes": notes}
    return render(request, "lab5_index.html", response)