from django.test import TestCase
from django.test import Client

# Create unit tests
class Lab2UnitTest(TestCase):
    def test_lab2_is_exist(self):
        response = Client().get("/lab-2/")
        self.assertEqual(response.status_code, 200)

    def test_xml_is_exist(self):
        response = Client().get("/lab-2/xml")
        self.assertEqual(response.status_code, 200)

    def test_json_is_exist(self):
        response = Client().get("/lab-2/json")
        self.assertEqual(response.status_code, 200)
