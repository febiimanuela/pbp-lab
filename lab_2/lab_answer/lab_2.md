# Apakah perbedaan antara JSON dan XML?
## JSON (Java Script Object Notation)
- Adalah format pertukaran data JavaScript
- Struktur datanya adalah map (pasangan key dan value)
- Mendukung array
- Tidak mendukung namespace dan comment
- Tidak menggunakan tags, readability lebih mudah    
- Ukuran lebih kecil, tidak ada redudansi dari tags, cepat diparsing
## XML (eXtensible Markup Language) 
- Adalah markup language
- Struktur datanya adalah tree
- Tidak mendukung array sehingga perlu manipulasi, yakni semua elemen dalam daftar tersebut harus dibuatkan tagnya satu per satu
- Mendukung namespace dan comment
- Menggunakan tags, readability lebih sulit
- Ukuran lebih besar, ada redudansi dari tags, lambat diparsing

# Apakah perbedaan antara HTML dan XML?
## HTML (Hyper Text Markup Language)
- Adalah gabungan hypertext dan predefined markup language
- Menampilkan data secara terstruktur dalam halaman web
- Bersifat statis karena hanya untuk menampilkan data
- Tag tidak fleksibel dan terbatas sebab sudah ditentukan oleh HTML
- Tag bersifat case insensitive
- Tidak semua menggunakan tag tutup
- Tidak mempertahankan whitespace
## XML (eXtensible Markup Language) 
- Adalah markup language yang dapat di-defined sendiri
- Menyimpan data dari halaman web
- Bersifat dinamis karena perlu mentransfer data
- Tag fleksibel dan tidak terbatas sebab ditentukan oleh kita sendiri
- Tag bersifat case sensitive
- Semua menggunakan tag tutup
- Mempertahankan whitespace

___

### Referensi: 
GeeksforGeeks. (2021, August 25). HTML vs XML. https://www.geeksforgeeks.org/html-vs-xml/
Shankar, R. (2021, January 7). JSON vs XML in 2021: Comparison, Features & Example. Hackr.Io. https://hackr.io/blog/json-vs-xml
Vats, R. (2021, April 3). HTML Vs XML: Difference Between HTML and XML [2021]. UpGrad Blog. https://www.upgrad.com/blog/html-vs-xml/
