from django.contrib import admin
from .models import Note

# Register Note models
admin.site.register(Note)